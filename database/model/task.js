const mongoose = require('../connection/connection');
const Moment = require("moment-timezone");

const Schema = mongoose.Schema;

const TaskSchema = new Schema({
    taskName: { type: String },
    taskTitle: { type: String },
    taskSubTitle: { type: String },
    taskDeadLine: { type: Date },
    taskAssignedUser: { type: String},
    isCompleted: { type: Boolean}
});

const Task = mongoose.model('task', TaskSchema);
module.exports =  Task;