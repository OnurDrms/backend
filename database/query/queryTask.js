const Task = require("../model/Task");

var queryTask = {
    findAll: function() {
       return Task.find().exec();
    },

    remove: function(id) {
        return Task.remove({_id: id}).exec();
    },
    findOneAndUpdate: function(taskName,isCompleted) {
        return Task.findOneAndUpdate({taskName: taskName}, {isCompleted: isCompleted}, {upsert: true}, function (err, doc) {
            if (err) {
                throw err;
            } else {
                console.log("Updated");
            }
        });
    },
    findOne: function(id) {
        return Task.findOne({_id: id}).exec();
    },
    insert:  async function(taskName,taskTitle,taskSubTitle,taskDeadLine,taskAssignedUser,isCompleted) {
       await Task.create({
           taskName: taskName,
           taskTitle: taskTitle,
           taskSubTitle: taskSubTitle,
           taskDeadLine: taskDeadLine,
           taskAssignedUser: taskAssignedUser,
           isCompleted: isCompleted
        });
    },
};
module.exports = queryTask;