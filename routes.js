
const queryTask = require('./database/query/queryTask');


module.exports = function(app) {

    app.post('/add/task', async (req,res ) => {

        await queryTask.insert(req.body.data.taskName, req.body.data.taskTitle,req.body.data.taskSubTitle,req.body.data.taskDeadLine,req.body.data.taskAssignedUser,req.body.data.isCompleted);

    });

    app.get('/task/get',  async (req, res) => {
        await queryTask.findAll().then(data => {
            return res.status(201).send({
                success: 'true',
                message: 'task successfully',
                results: data
            });
        });
    });

    app.post('/edit/task', async (req,res ) => {

        await queryTask.findOneAndUpdate(req.body.data.taskName, req.body.data.isCompleted);

    });
};
