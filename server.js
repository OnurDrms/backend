const express = require('express');
const cors = require('cors');
const app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next()
});


app.use(cors({credentials: true, origin: 'http://localhost:3000'}));
require('./routes')(app);
app.listen(8080);


